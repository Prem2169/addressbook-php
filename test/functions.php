<?php

/**
 * Implement db_connect
 * provides the connection if already exists otherwise creates a new connection and returns it
 * @return connection or an error
 */

 function db_connect()
 {
     static $connection;

     if(!isset($connection))
     {
         $config = parse_ini_file("config.ini");
         $connection = mysqli_connect($config['host'], $config['username'], $config['password'], $config['dbname']);
     }

     if($connection === false)
     {
         return mysqli_connect_error();
     }

     return $connection;
 }

 function db_query($query)
 {
     //connect to the database
     $connection = db_connect();

     $result = mysqli_query($connection, $query);

     return $result;
 }

 function db_error(){

 }

 function sanitize_input($value){
     $connection = db_connect();
     mysqli_real_escape_string($connection, $value);
 }

?>