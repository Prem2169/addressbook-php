<?php
require_once "includes/functions.php";

$q = $_REQUEST['q'];
$hint = null;
$hint = array();

$query = "SELECT * FROM contacts";
$result = db_select($query);

// echo "RESULT IS PRINTED";
// var_dump($result);

// lookup all hints from array if $q is different from ""
// var_dump( $hint);
if ($q !== "") {
    $q = strtolower($q);
    $len=strlen($q);
    foreach($result as $name) {
        //  || stristr($q, substr($name['last_name'], 0, $len)) || stristr($q, substr($name['address'], 0, $len))
        if (stristr($q, substr($name['first_name'], 0, $len)) || stristr($q, substr($name['last_name'], 0, $len)) || stristr($q, substr($name['address'], 0, $len)) ) {
            if ($hint == null) {
                $hint[] = $name;
            } else {
                $hint[] =  $name;
            }
        }
    }
}

// Output "no suggestion" if no hint was found or output correct values
var_dump( $hint == null ? $result : $hint);

?>