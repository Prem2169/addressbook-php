<?php
require_once "includes/functions.php";
$error_flag = false;

if(isset($_POST['action']))
{
    $first_name = sanitize_input($_POST['first_name']);
    
    $last_name = sanitize_input($_POST['last_name']);
    $email = sanitize_input($_POST['email']);
    
    $birthdate = sanitize_input($_POST['birthdate']);
    $birthdate = date('Y-m-d', strtotime($birthdate));
    //the above line converts the string into the date format
    
    $telephone = sanitize_input($_POST['telephone']);
    $address = sanitize_input($_POST['address']);

    $image_name = strtolower($first_name . "-" . $last_name);
    print($_POST['first_name']);
    // print($last_name);
    // print($email);
    // print($birthdate);
    // print($telephone);
    // print_r($_POST);
    if(isset($_FILES['pic']['name']))
    {
    /**
     * $_FILES is actually a global variable
     * $_FILES is a two dimensional associative global array of items which are being uploaded by via HTTP POST method and holds the attributes of files such as:

    * ATTRIBUTE	DESCRIPTION
     *[name]	Name of file which is uploading
     *[size]	Size of the file
     *[type]	Type of the file (like .pdf, .zip, .jpeg…..etc)
     *[tmp_name]	A temporary address where the file is located before processing the upload request
     *[error]	Types of error occurred when the file is uploading
    */
        $file_name = $_FILES['pic']['name'];
        //here in the above line "pic" is the value that is given inside the name attribute of the input image
        /**
         *  for eg: <input type="file" name="pic" id="pic" data-error=".pic_error">
         * 
         * here the name attribute has the value pic.
         */ 
        $file_tmp = $_FILES['pic']['tmp_name'];

        $temp = explode(".", $file_name);
        //The explode() function breaks a string into an array.

        $file_extension = strtolower(end($temp));
        //end is the array function which gives the end i.e last value in array.

        $image_name .= "." . $file_extension;
        move_uploaded_file($file_tmp, "images/users/$image_name");
        /**
         * The above line uploads the file into our local system 
         *  as this is the add contact so if i add contact there would be image with it so that image is saved into our local system 
         * and if there is already the same name in our system previously then it will replace that.
         */
    }//end of if

    $query = "INSERT INTO `contacts`(`first_name`, `last_name`, `email`, `birthdate`, `telephone`, `address`, `image_name`) VALUES ('{$first_name}', '{$last_name}', '{$email}', '{$birthdate}', '{$telephone}', '{$address}', '$image_name')";

    $result = db_query($query);

    if($result)
    {
        header("Location: index.php?q=success&op=insert");
        // print("everything done");
        // var_dump($result);
        // print_r($result);
    }
    else
    {
        $error_flag = true;
    }
}
?>
<!DOCTYPE html>
<html>

<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection" />

    <!--Import Csutom CSS-->
    <link rel="stylesheet" href="css/style.css" type="text/css">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>Add Contact</title>
</head>

<body>
    <!--NAVIGATION BAR-->
    <nav>
        <div class="nav-wrapper">
            <!-- Dropdown Structure -->
            <ul id="dropdown1" class="dropdown-content">
                <li><a href="#!">Profile</a></li>
                <li><a href="#!">Signout</a></li>
            </ul>
            <nav>
                <div class="nav-wrapper">
                    <a href="#!" class="brand-logo center">Contact Info</a>
                    <ul class="right hide-on-med-and-down">

                        <!-- Dropdown Trigger -->
                        <li><a class="dropdown-trigger" href="#!" data-target="dropdown1"><i
                                    class="material-icons right">more_vert</i></a></li>
                    </ul>
                </div>
            </nav>
            <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        </div>
    </nav>
    <!--/NAVIGATION BAR-->
    <div class="container">
        <div class="row mt50">
            <h2>Add New Contact</h2>
        </div>
<?php
        if($error_flag):
?>
        <div class="row">
            <!-- <div class="materialert">
                <i class="material-icons">check_circle</i> <span>Bienvenido, Linebeck</span>
                <button type="button" class="close-alert">×</button>
            </div>
            <div class="materialert info">
                <div class="material-icons">info_outline</div>
                Oh! What a beautiful alert :)
                <button type="button" class="close-alert">×</button>
            </div> -->
            <div class="materialert error">
                <div class="material-icons">error_outline</div>
                There were some errors while inserting record! Please retry again!
                <button type="button" class="close-alert">×</button>
            <!-- </div>
            <div class="materialert success">
                <div class="material-icons">check</div>
                Oh! What a beautiful alert :)
                <button type="button" class="close-alert">×</button>
            </div>
            <div class="materialert warning">
                <div class="material-icons">warning</div>
                Oh! What a beautiful alert :)
                <button type="button" class="close-alert">×</button>
            </div> -->
        </div>
<?php
    endif;
?>
        <div class="row">
            <form class="col s12 formValidate" action="<?= $_SERVER['PHP_SELF'];?>" id="add-contact-form" method="POST" enctype="multipart/form-data">
            <!--
                FROM THE ABOVE LINE: 

                * $_SERVER is a PHP super global variable which holds information about headers, paths, and script locations.

            $_SERVER['PHP_SELF']	Returns the filename of the currently executing script
             -->
                <div class="row mb10">
                    <div class="input-field col s6">
                        <input id="first_name" name="first_name" type="text" class="validate" data-error=".first_name_error">
                        <label for="first_name">First Name</label>
                        <div class="first_name_error "></div>
                    </div>
                    <div class="input-field col s6">
                        <input id="last_name" name="last_name" type="text" class="validate" data-error=".last_name_error">
                        <label for="last_name">Last Name</label>
                        <div class="last_name_error "></div>
                    </div>
                </div>
                <div class="row mb10">
                    <div class="input-field col s6">
                        <input id="email" name="email" type="email" class="validate" data-error=".email_error">
                        <label for="email">Email</label>
                        <div class="email_error "></div>
                    </div>
                    <div class="input-field col s6">
                        <input id="birthdate" name="birthdate" type="text" class="datepicker" data-error=".birthday_error">
                        <label for="birthdate">Birthdate</label>
                        <div class="birthday_error "></div>
                    </div>
                </div>
                <div class="row mb10">
                    <div class="input-field col s12">
                        <input id="telephone" name="telephone" type="tel" class="validate" data-error=".telephone_error">
                        <label for="telephone">Telephone</label>
                        <div class="telephone_error "></div>
                    </div>
                </div>
                <div class="row mb10">
                    <div class="input-field col s12">
                        <textarea id="address" name="address" class="materialize-textarea" data-error=".address_error"></textarea>
                        <label for="address">Addess</label>
                        <div class="address_error "></div>
                    </div>
                </div>
                <div class="row mb10">
                    <div class="file-field input-field col s12">
                        <div class="btn">
                            <span>Image</span>
                            <input type="file" name="pic" id="pic" data-error=".pic_error">
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path validate" type="text" placeholder="Upload Your Image">
                        </div>
                        <div class="pic_error "></div>
                    </div>
                </div>
                <button class="btn waves-effect waves-light right" type="submit" name="action">Submit
                        <i class="material-icons right">send</i>
                    </button>
            </form>
        </div>
    </div>
    <footer class="page-footer p0">
        <div class="footer-copyright ">
            <div class="container">
                <p class="center-align">© 2020 Study Link Classes</p>
            </div>
        </div>
    </footer>
    <!--JQuery Library-->
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <!--JQuery Validation Plugin-->
    <script src="vendors/jquery-validation/validation.min.js" type="text/javascript"></script>
    <script src="vendors/jquery-validation/additional-methods.min.js" type="text/javascript"></script>
    <!--Include Page Level Scripts-->
    <script src="js/pages/add-contact.js"></script>
    <!--Custom JS-->
    <script src="js/custom.js" type="text/javascript"></script>
</body>

</html>